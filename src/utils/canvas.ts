import {GraphicalObject} from "../map"

export const getMousePosition = (e: MouseEvent, canvas: HTMLCanvasElement) => {
  const rect = canvas.getBoundingClientRect()
  return {
    x: e.clientX - rect.left,
    y: e.clientY - rect.top
  }
}

export const resetCanvas = (ctx: CanvasRenderingContext2D) => {
  ctx.resetTransform()
  ctx.fillStyle = "antiquewhite"
  ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)
}

export const addRect = (ctx: CanvasRenderingContext2D) => {
  ctx.save()
  ctx.fillStyle = "red"
  ctx.fillRect(0, 0, 100, 100)
  ctx.restore()
}

export const addReference = (ctx: CanvasRenderingContext2D, x: number, y: number, size: number) => {
  ctx.save()
  ctx.strokeStyle = "red"
  ctx.beginPath()
  ctx.moveTo(x - size, y)
  ctx.lineTo(x + size, y)
  ctx.moveTo(x, y - size)
  ctx.lineTo(x, y + size)
  ctx.stroke()
  ctx.fillStyle = "blue"
  ctx.fillText(`${x},${y}`, x - size, y - size)
  ctx.restore()
}

export const addMiddleCross = (ctx: CanvasRenderingContext2D) => {
  ctx.save()
  ctx.fillStyle = "blue"
  ctx.beginPath()
  ctx.moveTo(ctx.canvas.width / 2, 0)
  ctx.lineTo(ctx.canvas.width / 2, ctx.canvas.height)
  ctx.moveTo(0, ctx.canvas.height / 2)
  ctx.lineTo(ctx.canvas.width, ctx.canvas.height / 2)
  ctx.stroke()
  ctx.restore()
}

export const locationToPath2D = (location: GraphicalObject): Path2D => {
  const { type, coordinates } = location
  const path2D = new Path2D()
  if (type === "Polygon") {
    path2D.moveTo(coordinates[0][0], coordinates[0][1])
    for (let i = 1; i < coordinates.length; i++) {
      path2D.lineTo(coordinates[i][0], coordinates[i][1])
    }
  } else if (type === "Rectangle") {
    path2D.rect(
      coordinates[0][0],
      coordinates[0][1],
      coordinates[1][0] - coordinates[0][0],
      coordinates[3][1] - coordinates[0][1]
    )
  } else {
    console.error("A Graphical Object is not Polygon")
  }
  return path2D
}

export const rectToGraphObj = (xCoord: number, yCoord: number, width: number, height?: number): GraphicalObject => ({
  type: "Rectangle",
  coordinates: [
    [xCoord, yCoord],
    [xCoord + width, yCoord],
    [xCoord + width, yCoord + (height || width)],
    [xCoord, yCoord + (height || width)],
    [xCoord, yCoord]
  ]
})
