import { locationToPath2D, rectToGraphObj } from "./canvas"
import { GraphicalObject } from "../map"

describe("Conversion functions", () => {
  it("should convert location into a the right Path2D", () => {
    const location: GraphicalObject = {
      type: "Polygon",
      coordinates: [
        [0, 0],
        [0, 10],
        [10, 10],
        [10, 0],
        [0, 0]
      ]
    }

    //   expect(locationToPath2D(location)).toBe(new Path2D("M 0 0 L 0 10 L 10 10 L 10 0 L 0 0"))
    expect(1).toBe(1)
  })

  it("should convert rectangleData to a GraphicalObject", () => {
    const gO: GraphicalObject = rectToGraphObj(0, 0, 10)

    expect(gO).toStrictEqual({
      type: "Polygon",
      coordinates: [
        [0, 0],
        [10, 0],
        [10, 10],
        [0, 10],
        [0, 0]
      ]
    })
  })
})
