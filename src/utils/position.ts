import { update, position } from ".."

export const onPositionClick = (event: MouseEvent) => {
  const positionInfoElement: HTMLSpanElement = document.getElementById("positionInfo")!
  const newPositionElement: HTMLSpanElement = document.createElement("span")
  newPositionElement.id = "positionInput"

  const xInputElement: HTMLInputElement = document.createElement("input")
  const yInputElement: HTMLInputElement = document.createElement("input")
  xInputElement.className = yInputElement.className = "position-input"
  xInputElement.type = yInputElement.type = "number"

  const slashSpanElement: HTMLSpanElement = document.createElement("span")
  slashSpanElement.innerHTML = " / "

  const goButtonElement: HTMLButtonElement = document.createElement("button")
  goButtonElement.innerText = "Go"
  function goOnClick(event) {
    const xNewValue = xInputElement.value
    const yNewValue = yInputElement.value
    if (xNewValue !== "") {
      position.x = +xNewValue
    }
    if (yNewValue !== "") {
      position.y = +yNewValue
    }
    update()
    goButtonElement.removeEventListener("click", goOnClick)
    newPositionElement.replaceWith(positionInfoElement)
    positionInfoElement.addEventListener("click", onPositionClick)
  }
  goButtonElement.addEventListener("click", goOnClick)

  newPositionElement.append(xInputElement, slashSpanElement, yInputElement, goButtonElement)

  positionInfoElement.replaceWith(newPositionElement)
}
