import {addMiddleCross, getMousePosition, rectToGraphObj, resetCanvas} from "./utils/canvas"
import {onPositionClick} from "./utils/position"
import {Building} from "./map/Building"
import {MapElement} from "./map"

export interface Position {
  x: number
  y: number
}

let canvas: HTMLCanvasElement = document.getElementById("game") as HTMLCanvasElement
canvas.height = window.innerHeight
canvas.width = window.innerWidth
let ctx: CanvasRenderingContext2D = canvas.getContext("2d")!

let plusBtn = document.getElementById("zoom-in")!
let minusBtn = document.getElementById("zoom-out")!

const positionInfoHTML = document.getElementById("positionInfo")!
const zoomInfoHTML = document.getElementById("zoomInfo")!

export let position: Position = { x: 0, y: 0 }
// { x: canvas.width / 2, y: canvas.height / 2 }
let mp: Position | undefined = undefined
export let zoom: number = 100

const updateInfo = () => {
  zoomInfoHTML.innerHTML = Math.floor(zoom) + "%"
  positionInfoHTML.innerHTML = Math.floor(position.x) + "/" + Math.floor(position.y)
}

export const update = () => {
  updateInfo()
  draw()
}

const mapElements: Array<MapElement> = []
// const square = new Rectangle(rectToGraphObj(0, 0, 10))
// mapElements.push(square)

const img = new Image()
img.src = "https://i.imgur.com/0ryFIvH.png"
img.onload = () => {
  const houses: Array<Building> = []
  const nbElems = 24
  const elemPerRow: number = Math.ceil(Math.sqrt(nbElems))
  for (let i = 0; i < nbElems; i++) {
    houses.push(
        new Building(
            rectToGraphObj(img.width * (i % elemPerRow), Math.floor(i / elemPerRow) * img.height, img.width, img.height),
            img
        )
    )
  }
  mapElements.push(...houses)
  draw()
}

export const draw = () => {
  // Apply new Transform
  resetCanvas(ctx)
  addMiddleCross(ctx)
  ctx.translate(canvas.width / 2, canvas.height / 2)
  ctx.scale(zoom / 100, zoom / 100)
  ctx.translate(-position.x, -position.y)

  // Draw each map elem using the new transform
  mapElements.forEach(gO => gO.draw(ctx))
}

canvas.addEventListener("mousedown", function(event) {
  mp = getMousePosition(event, this)
})

canvas.addEventListener("mousemove", function(event) {
  let newMousePos: Position = getMousePosition(event, canvas)
  if (mp) {
    document.body.style.cursor = "grab"
    position = {
      x: position.x - (newMousePos.x - mp.x) / (zoom / 100),
      y: position.y - (newMousePos.y - mp.y) / (zoom / 100)
    }
    mp = newMousePos
    update()
  } else {
    // square.isHoveredOn(ctx, newMousePos)
    // house.isHoveredOn(ctx, newMousePos)
    mapElements.forEach(gO => gO.isHoveredOn(ctx, newMousePos))
    draw()
  }
})

canvas.addEventListener("mouseup", function(event) {
  if (document.body.style.cursor !== "grab") {
    const HoveredMapElements: Array<MapElement> = mapElements.filter(elem => elem.isHoveredOn(ctx, mp!))
    console.log(HoveredMapElements)
  } else {
    document.body.style.cursor = "default"
  }
  mp = undefined
})

canvas.addEventListener("wheel", function(event) {
  if (zoom > 10 && event.deltaY > 0) {
    zoom -= 10
  } else if (zoom < 200 && event.deltaY < 0) {
    zoom += 10
  }
  update()
})

minusBtn.addEventListener("click", function(event) {
  if (zoom > 10) {
    zoom -= 10
  }
  update()
})

plusBtn.addEventListener("click", function(event) {
  if (zoom < 200) {
    zoom += 10
  }
  update()
})

positionInfoHTML.addEventListener("click", onPositionClick)

updateInfo()
draw()
