import { MapElement, GraphicalObject, Position } from "."
import { rectToGraphObj, locationToPath2D } from "../utils/canvas"

export class Building extends MapElement {
  public hoveredOn: boolean
  public img: HTMLImageElement = new Image()

  constructor(location: GraphicalObject, img: HTMLImageElement) {
    super(location)
    this.img = img
    this.location = rectToGraphObj(
      location.coordinates[0][0],
      location.coordinates[0][1],
      this.img.width,
      this.img.height
    )
    this.path2D = locationToPath2D(location)
    this.hoveredOn = false
  }

  draw = (ctx: CanvasRenderingContext2D) => {
    ctx.save()
    ctx.drawImage(this.img, this.location.coordinates[0][0], this.location.coordinates[0][1])
    if (this.hoveredOn) ctx.fillStyle = "rgba(255, 255, 255, .5)"
    else ctx.fillStyle = "rgba(255, 255, 255, 0)"
    ctx.fill(this.path2D)
    ctx.restore()
  }

  isHoveredOn = (ctx: CanvasRenderingContext2D, mp: Position) => {
    if (ctx.isPointInPath(this.path2D, mp.x, mp.y)) {
      this.hoveredOn = true
    } else {
      this.hoveredOn = false
    }
    return this.hoveredOn
  }
}
