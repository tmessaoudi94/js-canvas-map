import { locationToPath2D } from "../utils/canvas"

export type Coordinate = [number, number]
export type RectangleCoordinates = [Coordinate, Coordinate, Coordinate, Coordinate, Coordinate]

export interface Position {
  x: number
  y: number
}

export interface GraphicalObject {
  type: "Point" | "Polygon" | "Rectangle"
  coordinates: Array<Coordinate> | Coordinate
}

export class MapElement {
  location: GraphicalObject

  path2D: Path2D

  constructor(location: GraphicalObject) {
    this.location = location
    this.path2D = locationToPath2D(location)
  }

  draw = (ctx: CanvasRenderingContext2D) => {}

  isHoveredOn = (ctx: CanvasRenderingContext2D, mp: Position) => {}
}
