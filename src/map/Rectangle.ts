import { MapElement, GraphicalObject, Position } from "."

export class Rectangle extends MapElement {
  public hoveredOn: boolean

  constructor(location: GraphicalObject) {
    super(location)
    this.hoveredOn = false
  }

  draw = (ctx: CanvasRenderingContext2D) => {
    ctx.save()

    if (this.hoveredOn) ctx.fillStyle = "rgba(255, 255, 255, .5)"
    else ctx.fillStyle = "red"
    ctx.fill(this.path2D)
    ctx.restore()
  }

  isHoveredOn = (ctx: CanvasRenderingContext2D, mp: Position) => {
    if (ctx.isPointInPath(this.path2D, mp.x, mp.y)) {
      this.hoveredOn = true
    } else {
      this.hoveredOn = false
    }
    return this.hoveredOn
  }
}
